;;; Comments : package stuff
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
;;(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
;;(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))
(require 'diminish)
(require 'bind-key)
(setq use-package-verbose t)
;; Misc options
(add-hook 'emacs-lisp-mode-hook 'prettify-symbols-mode)
(add-hook 'clojure-mode-hook 'prettify-symbols-mode)
;; Backup file settings
(setq
   backup-by-copying t      ; don't clobber symlinks
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)       ; use versioned backups
;; Save by default in ~/.saves folder
(push (cons "." "~/.saves") backup-directory-alist)
(setq column-number-mode t)
;; enable hs to hide/show code blocks
(add-hook 'c-mode-common-hook #'hs-minor-mode)
(global-set-key (kbd "RET") 'newline-and-indent)
;;automatically indent when press RET
(setq-default indent-tabs-mode nil)
;; set appearance of a tab that is represented by 4 spaces
(setq-default tab-width 4)

;; Bind F5 to compile mode
(global-set-key (kbd "<f5>") (lambda ()
                               (interactive)
                               (setq-local compilation-read-command nil)
                               (call-interactively 'compile)))


(use-package org
  :mode (("\\.org$" . org-mode))
  :ensure org-plus-contrib
  :config
  (require 'ob-clojure)
  (require 'ob-dot)
  (require 'ob-plantuml)
  (setq org-babel-clojure-backend 'cider)
  (setq org-src-fontify-natively t)
  (setq org-plantuml-jar-path "~/bin/plantuml.jar")
  (setq org-ditaa-jar-path "~/bin/ditaa.jar")
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((clojure . t)
     (emacs-lisp . t)
     (java . t)
     (ditaa . t)
     ;;(c . t)
     (plantuml . t)
     )))
 

(use-package ag :ensure t)

(use-package which-key :ensure t
  :config
  (setq which-key-idle-delay 1.0)
  (setq which-key-max-description-length 27)
  (setq which-key-add-column-padding 0)
  (setq which-key-max-display-columns nil)
  (setq which-key-separator " → " )
  (setq which-key-unicode-correction 3)
  (setq which-key-prefix-prefix "+" )
  (setq which-key-special-keys nil)
  (setq which-key-show-prefix 'left)
  (setq which-key-show-remaining-keys nil)
  )

(use-package hl-line+ :ensure t)

;; (use-package uniquify :ensure t
;;   :config (setq uniquify-buffer-name-style 'forward))


(use-package cmake-ide :ensure t)

(use-package cmake-mode :ensure t)

(use-package cmake-font-lock :ensure t)
(use-package cc-mode
  :config
  (progn
    (add-hook 'c-mode-hook (lambda () (c-set-style "bsd")))
    (add-hook 'java-mode-hook (lambda () (c-set-style "bsd")))
    (setq tab-width 2)
    (setq c-basic-offset 2)))


(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :init
  (global-undo-tree-mode)
  )

(setq-default indent-tabs-mode nil)

(use-package whitespace
  :ensure t
  :diminish whitespace-mode
  :init
  (add-hook 'prog-mode-hook 'whitespace-mode)
  )


(use-package saveplace
  :ensure t
  :init
  (progn
    (setq-default save-place t)
    (setq save-place-file "~/.emacs.d/.saved-places")
    )
  :config
  (setq save-place-ignore-files-regexp
        (format "\\(%s\\)\\|\\(%s\\)"
                save-place-ignore-files-regexp
                tramp-file-name-regexp))
  )
(use-package subword
  :diminish subword-mode
  :init
  (global-subword-mode)
  )

(use-package ace-jump-mode
  :ensure t
  :demand
  :bind ("C-c c" . ace-jump-char-mode))

(use-package multiple-cursors
  :ensure t
  :bind ("C-S-C C-S-C" . mc/edit-lines)
  )


(use-package spacemacs-theme :ensure t)
(use-package zenburn-theme :ensure t)
(use-package solarized-theme :ensure t
 :init
  (progn
    (setq solarized-use-less-bold t
          solarized-use-more-italic t
          solarized-emphasize-indicators nil
          solarized-distinct-fringe-background nil
          solarized-high-contrast-mode-line nil))
  :config
  (progn
    (load "solarized-theme-autoloads" nil t)
    (setq theme-dark 'solarized-dark
          theme-bright 'solarized-light))
  )

(use-package smart-mode-line :ensure t
  :config
  (sml/apply-theme 'dark)
  :init
  (progn
    (setq sml/no-confirm-load-theme t)
    (sml/setup)))

(use-package sr-speedbar :ensure t)
;; MAGIT stuff
(use-package magit
  :ensure t
  :diminish auto-revert-mode
  :commands (magit-status magit-checkout)
  :bind (("C-x g" . magit-status))
  :init
  (setq magit-revert-buffers 'silent
        magit-push-always-verify nil
        git-commit-summary-max-length 70))

(use-package git-timemachine :ensure t)

;; PROJECTILE 
(use-package projectile
  :ensure t
  :commands (projectile-find-file projectile-switch-project)
  :diminish projectile-mode
  :init
  (use-package helm-projectile
    :ensure t
    :bind (("s-p" . helm-projectile-find-file)
           ("s-P" . helm-projectile-switch-project)))
  :config
  (projectile-global-mode))

;; COMPANY 
(use-package company
  :diminish company-mode
  :ensure t
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  ;;(global-company-mode 1)

  :config
  (progn
    (setq company-backends
          '(company-ycmd
            company-bbdb
            company-nxml
            company-css
            company-eclim
            company-semantic
            company-xcode
            ;; company-ropemacs
            company-cmake
            company-capf
            (company-dabbrev-code company-gtags company-etags company-keywords)
            company-oddmuse
            company-files
            company-dabbrev)))

  ;; Use Helm to complete suggestions
  (define-key company-mode-map (kbd "C-:") 'helm-company)
  (define-key company-active-map (kbd "C-:") 'helm-company)
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous))

(use-package company-c-headers :ensure t)

(use-package company-irony :ensure t)

(use-package company-irony-c-headers :ensure t
  :init
  (add-to-list 'company-backends 'company-c-headers)
  )

;; (use-package smartparens :ensure t
;;   :init
;;   (progn
;;     (use-package smartparens-config)
;;    ;; (use-package smartparens-ruby)
;;     (use-package smartparens-html)
;;     (smartparens-global-mode 1)
;;     (show-smartparens-global-mode 1))
;;   :config
;;   (progn
;;     (setq smartparens-strict-mode t)
;;     (sp-local-pair 'emacs-lisp-mode "`" nil :when '(sp-in-string-p)))
;;   :bind
;;   (("C-M-k" . sp-kill-sexp-with-a-twist-of-lime)
;;    ("C-M-f" . sp-forward-sexp)
;;    ("C-M-b" . sp-backward-sexp)
;;    ("C-M-n" . sp-up-sexp)
;;    ("C-M-d" . sp-down-sexp)
;;    ("C-M-u" . sp-backward-up-sexp)
;;    ("C-M-p" . sp-backward-down-sexp)
;;    ("C-M-w" . sp-copy-sexp)
;;    ("M-s" . sp-splice-sexp)
;;    ("M-r" . sp-splice-sexp-killing-around)
;;    ("C-)" . sp-forward-slurp-sexp)
;;    ("C-}" . sp-forward-barf-sexp)
;;    ("C-(" . sp-backward-slurp-sexp)
;;    ("C-{" . sp-backward-barf-sexp)
;;    ("M-S" . sp-split-sexp)
;;    ("M-J" . sp-join-sexp)
;;    ("C-M-t" . sp-transpose-sexp)
;;   )
;;   )


;; CIDER 
(use-package cider :ensure t
  :commands (cider cider-connect cider-jack-in)
  :init
  (setq cider-auto-select-error-buffer t
        cider-repl-pop-to-buffer-on-connect nil
        cider-repl-use-clojure-font-lock t
        cider-repl-wrap-history t
        cider-repl-history-size 1000
        cider-repl-history-file ".cider-history"
        cider-show-error-buffer t
        nrepl-hide-special-buffers t
        nrepl-popup-stacktraces nil)
  ;; legacy function now useless
  ;;(add-hook 'cider-mode-hook 'cider-turn-on-eldoc-mode)
  (add-hook 'cider-repl-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'cider-repl-mode-hook 'smartparens-strict-mode)
  (add-hook 'cider-repl-mode-hook 'subword-mode)
  (add-hook 'cider-test-report-mode 'jcf-soft-wrap)
  ;; nrepl isn't based on comint
  (add-hook 'cider-repl-mode-hook
            (lambda () (setq show-trailing-whitespace nil)))

  :config
  (use-package slamhound))

;; ERC config
(use-package erc
  :commands erc
  :init
  (setq
   erc-hide-list '("JOIN" "PART" "QUIT")
   erc-insert-timestamp-function 'erc-insert-timestamp-left
   erc-timestamp-format "[%H:%M] "
   erc-timestamp-only-if-changed-flag nil
   erc-truncate-mode t)
  :config
  (add-hook
   'window-configuration-change-hook
   (lambda () (setq erc-fill-column (- (window-width) 2)))))

(use-package eshell
:commands eshell
  :init
  (setq
   eshell-buffer-shorthand t
   eshell-cmpl-ignore-case t
   eshell-cmpl-cycle-completions nil
   eshell-history-size 10000
   eshell-hist-ignoredups t
   eshell-error-if-no-glob t
   eshell-glob-case-insensitive t
   eshell-scroll-to-bottom-on-input 'all)
  :config
  (defun jcf-eshell-here ()
    (interactive)
    (eshell "here"))

  (defun pcomplete/sudo ()
    (let ((prec (pcomplete-arg 'last -1)))
      (cond ((string= "sudo" prec)
             (while (pcomplete-here*
                     (funcall pcomplete-command-completion-function)
                     (pcomplete-arg 'last) t))))))

  (add-hook 'eshell-mode-hook
            (lambda ()
              (define-key eshell-mode-map
                [remap eshell-pcomplete]
                'helm-esh-pcomplete)
              (define-key eshell-mode-map
                (kbd "M-p")
                'helm-eshell-history)
              (eshell/export "NODE_NO_READLINE=1"))))


(use-package hippie-expand
  :init
  (setq hippie-expand-try-functions-list
        '(try-complete-file-name-partially
          try-complete-file-name
          try-expand-dabbrev
          try-expand-dabbrev-all-buffers
          try-expand-dabbrev-from-kill))
  :bind
  ("M-/" . hippie-expand))


;; ido
(use-package ido
  ;;:disabled t
  :init
  (setq
   ido-auto-merge-work-directories-length 0
   ido-default-buffer-method 'selected-window
   ido-enable-flex-matching t
   ido-use-filename-at-point nil
   ido-use-virtual-buffers t)

  (ido-mode t)
  (ido-everywhere t)
  (use-package ido-vertical-mode :ensure t :init (ido-vertical-mode 1))
  (use-package ido-ubiquitous :ensure t :init (ido-ubiquitous-mode t))
  (use-package idomenu :ensure t)

  :config
  ;; Allow the same buffer to be open in different frames.
  ;;
  ;; http://www.reddit.com/r/emacs/comments/21a4p9/use_recentf_and_ido_together/cgbprem
  (add-hook
   'ido-setup-hook
   (lambda ()
     (define-key ido-completion-map [up] 'previous-history-element))))

(use-package recentf
  :init
  (recentf-mode 1)

  :config
  (setq
   recentf-max-saved-items 1000
   recentf-exclude '("/tmp/" "/ssh:")))

(use-package paredit
  :diminish paredit-mode
  :init
  (add-hook 'clojure-mode-hook 'enable-paredit-mode)
  (add-hook 'cider-repl-mode-hook 'enable-paredit-mode)
  (add-hook 'lisp-mode-hook 'enable-paredit-mode)
  (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
  (add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
  (add-hook 'ielm-mode-hook 'enable-paredit-mode)
  (add-hook 'json-mode-hook 'enable-paredit-mode))

(use-package paren-face :ensure t
  :init
  (global-paren-face-mode)
  :config
  (add-hook 'clojure-mode-hook (lambda () (setq paren-face-regexp "#?[](){}[]"))))

(use-package flycheck
  :ensure t
  ;; :init
  ;; (progn
  ;;   ;; Enable flycheck mode as long as we're not in TRAMP
  ;;   (add-hook
  ;;    'prog-mode-hook
  ;;    (lambda () (if (not (is-current-file-tramp)) (flycheck-mode 1))))
  ;;   )
  )
(use-package flycheck-pos-tip
  :ensure t
  :defer t
  :config
  (with-eval-after-load 'flycheck (flycheck-pos-tip-mode)))

;; Auctex
(use-package auctex
  :ensure t
  :mode ("\\.tex\\'" . latex-mode)
  :commands (latex-mode LaTeX-mode plain-tex-mode)
  :init
  (progn
    (add-hook 'LaTeX-mode-hook #'LaTeX-preview-setup)
    (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
    (add-hook 'LaTeX-mode-hook #'flyspell-mode)
    (add-hook 'LaTeX-mode-hook #'turn-on-reftex)
    (setq TeX-auto-save t
          TeX-parse-self t
          TeX-save-query nil
          TeX-PDF-mode t)
    ))

;; Use company-auctex
(use-package company-auctex
  :ensure t
  :config
  (company-auctex-init)
  )

(use-package web-mode
  :ensure t
  :config
  (progn
    ;; Enable web mode in the following modes
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.js?\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.jsx?\\'" . web-mode))
    ;; Set the content type to "jsx" for the following file extensions
    (setq web-mode-content-types-alist
          '(("jsx" . "\\.js[x]?\\'")))
    )
  )

(use-package erlang
  :ensure t
  )

(use-package yaml-mode
  :config
  (require 'yaml-mode))


(use-package clojure-mode-extra-font-locking :ensure t)

(use-package cider :ensure t
  ;;:pin melpa-stable
  :init (progn
          (setq nrepl-hide-special-buffers nil
                cider-repl-pop-to-buffer-on-connect nil
                cider-prompt-for-symbol nil
                nrepl-log-messages t
                cider-popup-stacktraces t
                cider-repl-popup-stacktraces t
                cider-auto-select-error-buffer t
                cider-repl-print-length 100
                cider-repl-history-file (expand-file-name "cider-history" user-emacs-directory)
                cider-repl-use-clojure-font-lock t
                cider-switch-to-repl-command 'cider-switch-to-relevant-repl-buffer)
          (add-hook 'clojure-mode-hook 'cider-mode))
  :config (progn
            ;;(diminish-major-mode 'cider-repl-mode "Ç»")
            (add-to-list 'same-window-buffer-names "*cider*")
            ;;(add-hook 'cider-repl-mode-hook 'lisp-mode-setup)
            (add-hook 'cider-connected-hook 'cider-enable-on-existing-clojure-buffers))
  :diminish " ç")


;;(use-package cider-eval-sexp-fu :ensure t)

(use-package clj-refactor :ensure t
  :init (add-hook 'clojure-mode-hook (lambda ()
                                       (clj-refactor-mode 1)
                                       (cljr-add-keybindings-with-prefix "C-c M-r")))
  :diminish "")

;;(use-package cljsbuild-mode :ensure t)


(use-package smex :ensure t :init
  (setq smex-save-file
        (expand-file-name ".smex-items" user-emacs-directory)))

(use-package smartscan  :ensure t)

(use-package dashboard  :ensure t)

(use-package irony  :ensure t)

(use-package company-irony  :ensure t)

(use-package company-irony-c-headers  :ensure t)

(use-package flycheck-irony  :ensure t)

(use-package rainbow-delimiters :ensure t
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package markdown-mode :ensure t
  :mode (("\\.markdown$" . markdown-mode)
         ("\\.md$" . markdown-mode)))

(use-package org :ensure t )
(use-package beacon :ensure t
  :config
  (beacon-mode 1)
  )

(use-package scala-mode
  :interpreter ("scala" . scala-mode))

(use-package clojure-mode
  :mode (("\\.edn$" . clojure-mode))
  :config
  (progn
    (define-clojure-indent
      (defroutes 'defun)
      (GET 2)
      (POST 2)
      (PUT 2)
      (DELETE 2)
      (HEAD 2)
      (ANY 2)
      (context 2)
      (let-routes 1))

    (define-clojure-indent
      (form-to 1))

    (define-clojure-indent
      (match 1)
      (are 2)
      (checking 2)
      (async 1))

    (define-clojure-indent
      (select 1)
      (insert 1)
      (update 1)
      (delete 1))

    (define-clojure-indent
      (run* 1)
      (fresh 1))

    (define-clojure-indent
      (extend-freeze 2)
      (extend-thaw 1))

    (define-clojure-indent
      (go-loop 1))

    (define-clojure-indent
      (this-as 1)
      (specify 1)
      (specify! 1))

    (setq clojure--prettify-symbols-alist
          '(("fn" . ?λ)))

    (defun toggle-nrepl-buffer ()
      "Toggle the nREPL REPL on and off"
      (interactive)
      (if (string-match "cider-repl" (buffer-name (current-buffer)))
          (delete-window)
        (cider-switch-to-repl-buffer)))

    (defun cider-save-and-refresh ()
      (interactive)
      (save-buffer)
      (call-interactively 'cider-refresh))
    (global-set-key (kbd "s-r") 'cider-save-and-refresh)))

(use-package nyan-mode :ensure t
  :init (nyan-mode 1)
  )

(use-package spaceline :ensure t)

(use-package yasnippet
  :init
  (yas-global-mode 1))

(use-package ox-reveal :ensure t)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Pro for Powerline" :foundry "ADBE" :slant normal :weight normal :height 120 :width normal)))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("9d91458c4ad7c74cf946bd97ad085c0f6a40c370ac0a1cbeb2e3879f15b40553" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" default)))
 '(package-selected-packages
   (quote
    (beacon zenburn-theme which-key web-mode use-package undo-tree sr-speedbar spacemacs-theme spaceline solarized-theme smex smartscan smartparens rainbow-delimiters paren-face nyan-mode markdown-mode magit idomenu ido-vertical-mode ido-ubiquitous hl-line+ helm-projectile git-timemachine flycheck-pos-tip flycheck-irony erlang dashboard company-irony-c-headers company-irony company-auctex clojure-mode-extra-font-locking clj-refactor ace-jump-mode))))
